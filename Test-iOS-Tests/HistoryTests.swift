//
//  Test_iOS_Tests.swift
//  Test-iOS-Tests
//
//  Created by Adrian Pislaru on 02/09/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import XCTest

@testable import Test_iOS
@testable import SwiftDate
@testable import RxSwift
@testable import RxCocoa

class HistoryTests: XCTestCase {
    
    var historyViewModel: HistoryViewModel?
    
    let disposeBag = DisposeBag()
    
    override func setUp() {
        self.historyViewModel = HistoryViewModel()
    }
    
    override func tearDown() {
        self.historyViewModel = nil
    }
    
    func testPrepareAndSave() {
        if let historyViewModel = historyViewModel {
            
            historyViewModel.ronValues.subscribe(onNext: { list in
                
                XCTAssertTrue(list.count == 10)
            }).disposed(by: disposeBag)
            
            historyViewModel.usdValues.subscribe(onNext: { list in
                
                XCTAssertTrue(list.count == 10)
            }).disposed(by: disposeBag)
            
            historyViewModel.bgnValues.subscribe(onNext: { list in
                
                XCTAssertTrue(list.count == 10)
            }).disposed(by: disposeBag)
            
            let currentDate = Date().convertTo(region: Region.current).toFormat("yyyy-MM-dd")
            let dict = [
                currentDate: [
                    "RON": 1.16,
                    "USD": 4.15,
                    "BGN": 6.62
                ]
            ]
            
            historyViewModel.prepareAndSave(dict)
        } else {
            
            XCTAssertTrue(false)
        }
    }
    
    func testPrepareLastDays() {
        if let historyViewModel = historyViewModel {
        
            XCTAssertTrue(historyViewModel.lastDays.count == 10)
        } else {
            
            XCTAssertTrue(false)
        }
    }
}
