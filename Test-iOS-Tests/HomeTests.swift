//
//  Test_iOS_Tests.swift
//  Test-iOS-Tests
//
//  Created by Adrian Pislaru on 02/09/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import XCTest

@testable import Test_iOS
@testable import RxSwift
@testable import RxCocoa

class HomeTests: XCTestCase {

    var homeViewModel: HomeViewModel?
    
    override func setUp() {
        self.homeViewModel = HomeViewModel()
    }

    override func tearDown() {
        self.homeViewModel = nil
    }

    func testPrepareAndSave() {
        let dict = [
            "EUR": 1.23,
            "RON": 4.16,
            "CHF": 4.22
        ]
        
        if let homeViewModel = homeViewModel {
            
            homeViewModel.prepareAndSave(dict)
            
            XCTAssertTrue(homeViewModel.exchangeRates.value.count == 3)
        } else {
            
            XCTAssertTrue(false)
        }
    }
    
    func testScheduleNextUpdate() {
        if let homeViewModel = homeViewModel {
            
            homeViewModel.scheduleNextUpdate()
            
            XCTAssertTrue(homeViewModel.timer.isValid)
        } else {
            
            XCTAssertTrue(false)
        }
    }
}
