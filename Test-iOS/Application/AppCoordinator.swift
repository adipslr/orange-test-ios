//
//  AppCoordinator.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 30/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

/// AppCoordinator - Handles the navigation in the app.
class AppCoordinator {
    
    /// Keep track of the main window.
    let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    /**
     Coordinator start point, presents the SplashScreen then the TabBar.
     */
    func start() -> Observable<Void> {
        let viewModel = SplashViewModel()
        let viewController = SplashViewController.instantiate()
        viewController.viewModel = viewModel
        
        window.rootViewController = viewController
        window.makeKeyAndVisible()
        
        self.setupTabBar(on: viewController)
        
        return .never()
    }
    
    /// Instantiate tabbar screens and present the tabbar over the rootViewController.
    private func setupTabBar(on rootViewController: UIViewController) {
        
        let HomeVC: UIViewController = {
            let viewModel = HomeViewModel()
            let vc = HomeViewController.instantiate()
            vc.viewModel = viewModel
            
            viewModel.setup()
            
            return vc;
        }();
        
        let HistoryVC: UIViewController = {
            let viewModel = HistoryViewModel()
            let vc = HistoryViewController.instantiate()
            vc.viewModel = viewModel
            
            viewModel.setup()
            
            return vc;
        }();
        
        let SettingsVC: UIViewController = {
            let viewModel = SettingsViewModel()
            let vc = SettingsViewController.instantiate()
            vc.viewModel = viewModel
            
            return vc;
        }();
        
        let mainTabBarController = UITabBarController();
        mainTabBarController.viewControllers = [HomeVC, HistoryVC, SettingsVC]
        mainTabBarController.viewControllers?.forEach { _ = $0.view }
        
        rootViewController.present(mainTabBarController, animated: true, completion: nil)
    }
}
