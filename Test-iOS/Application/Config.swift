//
//  Config.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 30/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import Foundation

struct Config {
    
    struct Network {
        
        static var apiBaseUrl: String {
            return "https://api.exchangeratesapi.io"
        }
    }
}
