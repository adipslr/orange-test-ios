//
//  DemoAPI.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 30/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import Foundation
import Moya

/// API endpoints.
public enum ExchangeRatesAPI {
    
    case latest(base: String)
    case history(start: String, end: String, symbols: String, base: String)
}

/// Implementing the protocol used to define the specifications necessary for a MoyaProvider.
extension ExchangeRatesAPI: TargetType {
    
    /// Base URL of the API.
    public var baseURL: URL {
        return URL(string: Config.Network.apiBaseUrl)!
    }
    
    /// Path for every endpoint.
    public var path: String {
        switch self {
        case .latest: return "/latest"
        case .history: return "/history"
        }
    }
    
    /// Method for every endpoint.
    public var method: Moya.Method {
        switch self {
        default:
            return .get
        }
    }
    
    /// Headers used in request.
    public var headers: [String: String]? {
        return nil
    }
    
    public var sampleData: Data {
        return "".data(using: .utf8)!
    }
    
    /// Request Config
    public var task: Task {
        switch self {
        case .latest(let base):
            return .requestParameters(parameters: ["base": base], encoding: URLEncoding.queryString)
            
        case .history(let start, let end, let symbols, let base):
            return .requestParameters(parameters: ["start_at": start,
                                                   "end_at": end,
                                                   "symbols": symbols,
                                                   "base": base],
                                      encoding: URLEncoding.queryString)
        }
    }
    
    public func url(route: TargetType) -> String {
        return route.baseURL.appendingPathComponent(route.path).absoluteString
    }
}
