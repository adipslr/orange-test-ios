//
//  ExchangeRate.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 31/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import Foundation

struct ExchangeRate: Codable {
    
    var currency: Currency
    var rate: Double
}
