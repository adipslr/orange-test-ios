//
//  Currency.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 31/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import Foundation

enum Currency: String, Codable, CaseIterable {
    
    case EUR
    case CAD
    case HKD
    case ISK
    case PHP
    case DKK
    case HUF
    case CZK
    case AUD
    case RON
    case SEK
    case IDR
    case INR
    case BRL
    case RUB
    case HRK
    case JPY
    case THB
    case CHF
    case SGD
    case PLN
    case BGN
    case TRY
    case CNY
    case NOK
    case NZD
    case ZAR
    case USD
    case MXN
    case ILS
    case GBP
    case KRW
    case MYR
}
