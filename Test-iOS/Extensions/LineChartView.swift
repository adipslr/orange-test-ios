//
//  LineChartView.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 31/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import Foundation
import Charts

extension LineChartView {
    
    /**
     Setup the chart with respect to the application specification.
     
     - Parameter data: Array of values which will be represented in the chart.
     */
    func setup(with data: [String]) {
        self.backgroundColor = .white
        
        self.setScaleEnabled(false)
        self.pinchZoomEnabled = false
        
        self.xAxis.setLabelCount(10, force: true) // Force showing 10 days on the xAxis
        self.xAxis.labelFont = .systemFont(ofSize: 10)
        self.xAxis.valueFormatter = BarChartFormatter(initWith: data)
       
        self.rightAxis.enabled = false
        
        self.legend.enabled = false
    }
    
    /**
     Updates the data in the chart.
     
     - Parameter data: New values which will be represented in the chart.
     - Parameter color: Color of the cubic bezier path of the chart.
     */
    func updateData(with data: [Double], and color: UIColor) {
        /*
         Map the values which will be shown in the app to ChartDataEntry object.
         The x value will be represented by the day, so will be set to the index value(day 0 to 9).
         The y value will be respresented by the value, the exchange rate at that time.
         */
        let entries = data.enumerated().map { ChartDataEntry(x: Double($0.offset), y: $0.element) }
        
        let dataSet = LineChartDataSet(entries: entries, label: "DataSet")
        dataSet.mode = .cubicBezier
        dataSet.drawCirclesEnabled = false
        dataSet.fillColor = color
        dataSet.fillAlpha = 1
        dataSet.drawFilledEnabled = true
        
        let chartData = LineChartData(dataSet: dataSet)
        chartData.setDrawValues(false)
        
        self.data = chartData
    }
}

/// Helper class for mapping the values from the xAxis of the chart to the corresponding string values.
private class BarChartFormatter: IAxisValueFormatter {
    
    var values: [String] = [String]()
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let intValue = Int(value)
        
        guard values.count >= (intValue + 1) else { return "" }
        
        return values[intValue]
    }
    
    /**
     - Parameter data: Array of string values which will be printed on the xAxis of the chart.
     */
    init(initWith data: [String]) {
        self.values = data
    }
}
