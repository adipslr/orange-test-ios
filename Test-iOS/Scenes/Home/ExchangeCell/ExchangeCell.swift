//
//  ExchangeCell.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 31/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import Foundation
import UIKit

class ExchangeCell: UITableViewCell {
    
    @IBOutlet weak var currencyNameLabel: UILabel!
    @IBOutlet weak var exchangeRateLabel: UILabel!
    
    func setupWith(_ rate: ExchangeRate) {
        currencyNameLabel.text = rate.currency.rawValue
        exchangeRateLabel.text = "\(rate.rate)"
    }
}
