//
//  HomeViewModel.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 30/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import RxSwift
import RxCocoa
import SwiftDate

class HomeViewModel {
    
    private let disposeBag = DisposeBag()
    
    var timer: Timer = Timer()
    
    private var refreshTime = StateManager.shared.refreshTime
    private var baseCurrency = StateManager.shared.baseCurrency
    
    // MARK: - Inputs
    
    /// Keeps track of the visibility of the screen.
    let visibilityChanged = BehaviorRelay<Bool>(value: false)
    
    // MARK: - Outputs
    
    let exchangeRates = BehaviorRelay<[ExchangeRate]>(value: [])
    
    let lastUpdateLabel = BehaviorRelay<String>(value: "Last update - never")
    let baseLabel = BehaviorRelay<String>(value: "Base currency - \(StateManager.shared.baseCurrency.rawValue)")
    
    init() {
        self.setup()
    }
    
    func setup() {
        /// Listen to state change event to update the labels in the screen.
        StateManager.shared.stateChanged.subscribe(onNext: { [unowned self] (refreshTime, baseCurrency) in
            
            self.refreshTime = refreshTime
            self.baseCurrency = baseCurrency
            
            self.baseLabel.accept("Base currency - \(baseCurrency.rawValue)")
        }).disposed(by: disposeBag)
        
        /// Listen to visibility changes for starting/stoping the auto-refresh.
        visibilityChanged.subscribe(onNext: { [unowned self] visible in
            
            // If screen becomes inactive, stop auto-refresh.
            self.timer.invalidate()
            
            // If screen becomes active, update and start auto-refresh.
            if visible {
                self.updateAndSchedule()
            }
            
        }).disposed(by: disposeBag)
    }
    
    /// Update the exchange rates and schedule next refresh.
    @objc private func updateAndSchedule() {
        NetworkManager.shared.provider.rx.request(.latest(base: baseCurrency.rawValue))
            .filterSuccessfulStatusCodes()
            .map([String: Double].self, atKeyPath: "rates")
            .subscribe(onSuccess: { [unowned self] dict in
                
                self.prepareAndSave(dict)
            })
            .disposed(by: disposeBag)
        
        
        self.scheduleNextUpdate()
    }
    
    /// Schedules the next refresh of the data.
    func scheduleNextUpdate() {
        self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(refreshTime),
                                          target: self,
                                          selector: #selector(updateAndSchedule),
                                          userInfo: nil,
                                          repeats: false)
    }
    
    /**
     Prepares the rawData from the API in order to be used in tableView.
     
     - Parameter data: dictionary from currency to rate.
     */
    func prepareAndSave(_ data: [String: Double]) {
        var rates = [ExchangeRate]()
        
        data.forEach({ (rawCurrency, rate) in
            
            if let currency = Currency(rawValue: rawCurrency) {
                rates.append(ExchangeRate(currency: currency, rate: rate))
            }
        })
        
        let sortedRates = rates.sorted(by: { $0.currency.rawValue < $1.currency.rawValue })
        
        self.exchangeRates.accept(sortedRates)
        
        let dateStr = Date().convertTo(region: Region.current).toFormat("dd/MM/yyyy HH:mm:ss")
        
        self.lastUpdateLabel.accept("Last update - \(dateStr)")
    }
}
