//
//  HomeViewController.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 30/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Reusable

class HomeViewController: UIViewController, StoryboardBased {
    
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lastUpdateLabel: UILabel!
    @IBOutlet weak var baseCurrencyLabel: UILabel!
    
    var viewModel: HomeViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupScreen()
        
        createViewModelBinding()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel.visibilityChanged.accept(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        viewModel.visibilityChanged.accept(false)
    }
    
    private func createViewModelBinding() {
        // Bind data to table.
        viewModel.exchangeRates.bind(to: tableView.rx.items(cellIdentifier: "ExchangeCell",
                                                            cellType: ExchangeCell.self)) { (_, item, cell) in
        
                                                                cell.setupWith(item)
        }.disposed(by: disposeBag)
        
        // Update labels when state changed.
        viewModel.baseLabel.subscribe(onNext: { [unowned self] baseCurrencyLabelText in
        
            self.baseCurrencyLabel.text = baseCurrencyLabelText
        }).disposed(by: disposeBag)
        
        viewModel.lastUpdateLabel.subscribe(onNext: { [unowned self] lastUpdateLabelText in
            
            self.lastUpdateLabel.text = lastUpdateLabelText
        }).disposed(by: disposeBag)
    }
    
    private func setupScreen() {
        self.title = "Home"
        self.tabBarItem.image = UIImage(named: "homeIcon")
        self.tableView.rowHeight = 44
    }
}
