//
//  HistoryViewModel.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 30/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import RxSwift
import RxCocoa
import SwiftDate

class HistoryViewModel {
    
    private let disposeBag = DisposeBag()
    
    private var baseCurrency = StateManager.shared.baseCurrency
    
    // MARK: - Inputs
    
    // MARK: - Outputs
    
    // Array of strings used on the xAxis of the charts.
    var lastDays: [String] = [String]()
    
    let baseLabel = BehaviorRelay<String>(value: "Base currency - \(StateManager.shared.baseCurrency.rawValue)")
    
    let ronValues = PublishRelay<[Double]>()
    let usdValues = PublishRelay<[Double]>()
    let bgnValues = PublishRelay<[Double]>()
    
    init() {
        self.setup()
    }
    
    func setup() {
        // Listen to state changes to update the labels and the values in the charts.
        StateManager.shared.stateChanged.subscribe(onNext: { [unowned self] (_, baseCurrency) in
            
            self.baseCurrency = baseCurrency
            
            self.updateValues()
            
            self.baseLabel.accept("Base currency - \(baseCurrency.rawValue)")
        }).disposed(by: disposeBag)
        
        setupLastDays()
        updateValues()
    }
    
    /// Update the values in the charts by making network calls to the API.
    private func updateValues() {
        let currentDate = Date().convertTo(region: Region.current).toFormat("yyyy-MM-dd")
        let pastDate = Date().convertTo(region: Region.current).dateByAdding(-9, .day).toFormat("yyyy-MM-dd")
        
        let endpoint: ExchangeRatesAPI = .history(start: pastDate,
                                                  end: currentDate,
                                                  symbols: "RON,USD,BGN",
                                                  base: baseCurrency.rawValue)
        
        NetworkManager.shared.provider.rx.request(endpoint)
            .filterSuccessfulStatusCodes()
            .map([String: [String: Double]].self, atKeyPath: "rates")
            .subscribe(onSuccess: { [unowned self] data in
                
                self.prepareAndSave(data)
            })
            .disposed(by: disposeBag)
    }
    
    /**
     Prepares the rawData from the API in order to be used in charts.
     
     - Parameter data: dictionary from dateString to dictionary of currencies:rates.
     */
    func prepareAndSave(_ data: [String: [String: Double]]) {
        /*
            RawArrays used to keep track of existing and non-existing values for each day.
            If the rate is unchanged since yesterday, the API won't send data for today.
            By using this arrays,
                it's easier to create the final data by copying missing information from neighbour values.
         */
        var ronData = [Double?](repeating: nil, count: 10)
        var usdData = [Double?](repeating: nil, count: 10)
        var bgnData = [Double?](repeating: nil, count: 10)
        
        let pastDate = Date().convertTo(region: Region.current).dateByAdding(-9, .day)
        
        // Populate RawArrays with existing data from the API.
        for i in 0...9 {
            let day = pastDate.dateByAdding(i, .day).toFormat("yyyy-MM-dd")
            
            if let dayDict = data[day] {
                ronData[i] = dayDict["RON"]
                usdData[i] = dayDict["USD"]
                bgnData[i] = dayDict["BGN"]
            }
        }
        
        /*
            Get the first non-null index ( the first day that has information )
                and update the days with no information by copying from neighbours.
        */
        if let index = ronData.firstIndex(where: { $0 != nil }) {
            
            // Update the values from the left of the index if possible.
            if index - 1 >= 0 {
                for i in (0...(index - 1)).reversed() where ronData[i] == nil {
                    ronData[i] = ronData[i + 1]
                    usdData[i] = usdData[i + 1]
                    bgnData[i] = bgnData[i + 1]
                }
            }
            
            // Update the values from the right of the index if possible.
            if index + 1 <= 9 {
                for i in ((index + 1)...9) where ronData[i] == nil {
                    ronData[i] = ronData[i - 1]
                    usdData[i] = usdData[i - 1]
                    bgnData[i] = bgnData[i - 1]
                }
            }
            
            // Send the final data to charts.
            ronValues.accept(ronData.map({ $0! }))
            usdValues.accept(usdData.map({ $0! }))
            bgnValues.accept(bgnData.map({ $0! }))
        }
    }
    
    /// Updates the lastDay variabile with the string representations of the last 10 days.
    func setupLastDays() {
        for i in 0...9 {
            
            let pastDay = Date().convertTo(region: Region.current).dateByAdding(-i, .day).toFormat("EEE")
            lastDays.insert(pastDay, at: 0)
        }
    }
}
