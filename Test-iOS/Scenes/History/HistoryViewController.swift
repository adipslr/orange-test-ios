//
//  HistoryViewController.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 30/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Reusable
import Charts

class HistoryViewController: UIViewController, StoryboardBased {
    
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var baseCurrencyLabel: UILabel!
    
    @IBOutlet weak var ronChart: LineChartView!
    @IBOutlet weak var usdChart: LineChartView!
    @IBOutlet weak var bgnChart: LineChartView!
    
    var viewModel: HistoryViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupScreen()
        self.setupCharts()
        
        createViewModelBinding()
    }
    
    private func createViewModelBinding() {
        // Update base currency label if state was changed.
        viewModel.baseLabel.subscribe(onNext: { [unowned self] baseCurrencyLabelText in
            
            self.baseCurrencyLabel.text = baseCurrencyLabelText
        }).disposed(by: disposeBag)
        
        // Update chart values.
        viewModel.ronValues.subscribe(onNext: { [unowned self] values in
            
            self.ronChart.updateData(with: values, and: .red)
        }).disposed(by: disposeBag)
        
        viewModel.usdValues.subscribe(onNext: { [unowned self] values in
            
            self.usdChart.updateData(with: values, and: .green)
        }).disposed(by: disposeBag)
        
        viewModel.bgnValues.subscribe(onNext: { [unowned self] values in
            
            self.bgnChart.updateData(with: values, and: .blue)
        }).disposed(by: disposeBag)
    }
    
    private func setupScreen() {
        self.title = "History"
        self.tabBarItem.image = UIImage(named: "historyIcon")
    }
    
    private func setupCharts() {
        ronChart.setup(with: viewModel.lastDays)
        usdChart.setup(with: viewModel.lastDays)
        bgnChart.setup(with: viewModel.lastDays)
    }
}

