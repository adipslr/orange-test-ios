//
//  SettingsViewController.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 30/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Reusable

class SettingsViewController: UIViewController, StoryboardBased {
    
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var refreshTimeText: UITextField!
    @IBOutlet weak var baseCurrencyText: UITextField!
    
    let refreshPicker = UIPickerView()
    let basePicker = UIPickerView()
    
    var viewModel: SettingsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupScreen()
        self.setupPickers()
        
        createViewModelBinding()
        
        self.updateRefreshTimeText()
        self.updateBaseCurrencyText()
    }
    
    private func createViewModelBinding() {
        // Bind picker values.
        viewModel.refreshTimes.bind(to: refreshPicker.rx.itemTitles) { "\($1)" }.disposed(by: disposeBag)
        viewModel.currencies.bind(to: basePicker.rx.itemTitles) { $1.rawValue }.disposed(by: disposeBag)
        
        // Prevent keyboard typing in textfields.
        refreshTimeText.rx.text.orEmpty.scan("") { (_, _) -> String in
            
            return "\(StateManager.shared.refreshTime)"
        }
        .subscribe(refreshTimeText.rx.text)
        .disposed(by: disposeBag)
        
        baseCurrencyText.rx.text.orEmpty.scan("") { (_, _) -> String in
            
            return StateManager.shared.baseCurrency.rawValue
        }
        .subscribe(baseCurrencyText.rx.text)
        .disposed(by: disposeBag)
        
        // Update state whenever new value is selected in the picker.
        refreshPicker.rx.itemSelected.subscribe(onNext: { [unowned self] in
            
            StateManager.setRefreshTimeTo(self.viewModel.refreshTimes.value[$0.row])
        }).disposed(by: disposeBag)
        
        basePicker.rx.itemSelected.subscribe(onNext: { [unowned self] in
            
            StateManager.setBaseCurrencyTo(self.viewModel.currencies.value[$0.row])
        }).disposed(by: disposeBag)
        
        // Listen to state changes for updating the pickers.
        StateManager.shared.stateChanged.subscribe(onNext: { [unowned self] (refreshTime, baseCurrency) in
            
            self.updateRefreshTimeText()
            self.updateBaseCurrencyText()
        }).disposed(by: disposeBag)
    }
    
    private func setupScreen() {
        self.title = "Settings"
        self.tabBarItem.image = UIImage(named: "settingsIcon")
    }
    
    private func setupPickers() {
        // Assign pickers to textfields.
        refreshTimeText.inputView = refreshPicker
        baseCurrencyText.inputView = basePicker
        
        // Create toolbar accesory for Done button.
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done",
                                         style: .done,
                                         target: self,
                                         action: #selector(self.closePickers))
        
        // Blank space to move Done button on the right side.
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([flexibleSpace, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        refreshTimeText.inputAccessoryView = toolBar
        baseCurrencyText.inputAccessoryView = toolBar
    }
    
    @objc private func closePickers() {
        refreshTimeText.resignFirstResponder()
        baseCurrencyText.resignFirstResponder()
    }
    
    /// Update refresh time label & picker selection.
    private func updateRefreshTimeText() {
        self.refreshTimeText.text = "\(StateManager.shared.refreshTime)"
        
        let selectedIndex = viewModel.refreshTimes.value.firstIndex(of: StateManager.shared.refreshTime) ?? 0
        self.refreshPicker.selectRow(selectedIndex, inComponent: 0, animated: false)
    }
    
    /// Update base currency label & picker selection.
    private func updateBaseCurrencyText() {
        self.baseCurrencyText.text = StateManager.shared.baseCurrency.rawValue
        
        let selectedIndex = viewModel.currencies.value.firstIndex(of: StateManager.shared.baseCurrency) ?? 0
        self.basePicker.selectRow(selectedIndex, inComponent: 0, animated: false)
    }
}
