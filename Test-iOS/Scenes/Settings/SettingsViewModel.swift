//
//  SettingsViewModel.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 30/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import RxSwift
import RxCocoa

class SettingsViewModel {
    
    private let disposeBag = DisposeBag()
    
    // MARK: - Inputs
    
    // MARK: - Outputs
    
    let refreshTimes = BehaviorRelay<[Int]>(value: [3, 5, 15])
    let currencies = BehaviorRelay<[Currency]>(value: Currency.allCases)
    
    init() {
        
    }
}

