//
//  SplashViewController.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 30/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Reusable

class SplashViewController: UIViewController, StoryboardBased {
    
    var viewModel: SplashViewModel!
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createViewModelBinding()
    }
    
    func createViewModelBinding() {
        
    }
}
