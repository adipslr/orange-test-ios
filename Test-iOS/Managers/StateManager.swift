//
//  StateManager.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 31/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import Foundation
import Defaults
import RxSwift

extension Defaults.Keys {
    
    static let refreshTime = Key<Int>("refreshTime", default: 3)
    static let baseCurrency = Key<Currency>("baseCurrency", default: Currency.EUR)
}

/// State Manager - used to keep track of the important settings in the app.
class StateManager {
    
    /// Shared instance used all over the app.
    static let shared = StateManager()
    
    /// Event fired when state was changed.
    let stateChanged = PublishSubject<(Int, Currency)>()
    
    /*
     Var used to keep track of the refresh time of HomeScreen.
     Whenever setter is used, the value is also saved in UserDefaults.
     */
    var refreshTime: Int {
        get {
            return defaults[.refreshTime]
        }
        
        set {
            defaults[.refreshTime] = newValue
            stateChanged.onNext((refreshTime, baseCurrency)) /// Fire state changed event.
        }
    }
    
    /*
     Var used to keep track of the base currency used in the app.
     Whenever setter is used, the value is also saved in UserDefaults.
     */
    var baseCurrency: Currency {
        get {
            return defaults[.baseCurrency]
        }
        
        set {
            defaults[.baseCurrency] = newValue
            stateChanged.onNext((refreshTime, newValue)) /// Fire state changed event.
        }
    }
    
    /// Updates the refresh time in the entire app.
    class func setRefreshTimeTo(_ seconds: Int) {
        StateManager.shared.refreshTime = seconds
    }
    
    /// Updates the base currency in the entire app.
    class func setBaseCurrencyTo(_ currency: Currency) {
        StateManager.shared.baseCurrency = currency
    }
}
