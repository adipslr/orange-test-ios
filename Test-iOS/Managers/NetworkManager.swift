//
//  NetworkManager.swift
//  Test-iOS
//
//  Created by Adrian Pislaru on 30/08/2019.
//  Copyright © 2019 Tremend Software Consulting S.R.L. All rights reserved.
//

import Foundation
import Moya

/**
    Network Manager
    Used Moya, wrapper over Alamofire, because it is more useful in the MVVM arhitecture.
 */
class NetworkManager {
    
    /// Shared instance used all over the app.
    static let shared = NetworkManager()
    
    /// Moya Provider, requests should be made only trough this var.
    var provider = MoyaProvider<ExchangeRatesAPI>()
    
    init() {
        self.provider = MoyaProvider<ExchangeRatesAPI>()
    }
}
